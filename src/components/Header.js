import { Link } from "react-router-dom";
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
function Header(){
  return (
    <nav className="navbar navbar-expand navbar-dark bg-dark">
    <a href="/blog-challenge" className="navbar-brand">
      Home
    </a>
    <div className="navbar-nav mr-auto">
      <li className="nav-item">
        <Link to={"/blog-challenge/newPost"} className="nav-link">
          New Post
        </Link>
      </li>
     </div>
     </nav>  
  );
};

export default Header;