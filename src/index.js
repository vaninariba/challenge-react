import React from "react";
import ReactDOM from "react-dom";
import Home from "./views/home";
import Header from "./components/Header";
import Single from "./views/single";
import New from "./views/new";
import Update from "./views/update"
import { BrowserRouter, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";


ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Header />
      <Route exact path="/blog-challenge" component={Home} />
      <Route exact path="/blog-challenge/newPost" component={New} />
      <Route exact path="/blog-challenge/updatePost/:id" component={Update} />
      <Route exact path="/blog-challenge/posts/:id" component={Single} />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);