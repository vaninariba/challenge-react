import { Fragment, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import styles from "./css/styles.css";
import { fetchOnePost } from "../reducers/postsReducer";

const Single = ({ match }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchOnePost(match.params.id));
  }, [dispatch, match.params.id]);

  const { singlePost } = useSelector((state) => state.posts);
  return (
    <Fragment>
      <div className={styles.mainContainer}>
        <div className={styles.postContainer}>
          <h3>{singlePost.id}</h3>
          <h2>{singlePost.title}</h2>
          <p>{singlePost.body}</p>
        </div>
      </div>
    </Fragment>
  );
};

export default Single;