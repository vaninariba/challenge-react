import { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  updatePost,
  fetchOnePost,
  fetchAllPosts,
} from "../reducers/postsReducer";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Swal from "sweetalert2";

const Update = ({ match }) => {
  const dispatch = useDispatch();
  const [error, setError] = useState(true);
  const [input, setInput] = useState({
    title: "",
    body: "",
  });

  const { singlePost } = useSelector((state) => state.posts);

  useEffect(() => {
    dispatch(fetchAllPosts()).then(() =>
      dispatch(fetchOnePost(match.params.id))
    );
  }, [dispatch, match.params.id]);

  const handleChange = (e) => {
    if (input.body.length >= 5 && input.title.length >= 5) {
      setError(false);
    } else {
      setError(true);
    }
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(
      updatePost({ title: input.title, body: input.body, id: singlePost.id })
    ).then(() => {
      Swal.fire(
        "Post Updated!",
        "The post was updated.",
        "success"
      );
      setInput({ title: "", body: "" });
    });
  };

  return (
    <Fragment>
      <h3>Update Post</h3>
      <div className="card text-white bg-secondary mb-3">
        <form onSubmit={handleSubmit}>
          <div className="mb-3 row">
            <label className="col-sm-2 col-form-label">Title</label>
          </div>
          <div className="col-sm-10">
            <input value={input.title}
              placeholder={singlePost.title}
              name="title"
              onChange={handleChange}
              type="text"
              min="5"
              max="20" />
          </div>
          <div className="mb-3 row">
            <label className="col-sm-2 col-form-label">Body</label>
          </div>
          <div className="col-sm-10">
            <textarea
              value={input.body}
              placeholder={singlePost.body}
              name="body"
              onChange={handleChange}
              type="text"
              min="5"
              max="200"
            />
          </div>
          <button
            disabled={
              error
            }
            type="submit"
            className="btn btn-success"
          >
            Update Post
          </button>
        </form>
      </div>
    </Fragment>
  );
};
export default Update;
