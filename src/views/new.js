import { Fragment, useState } from "react";
import { useDispatch } from "react-redux";
import { createPost } from "../reducers/postsReducer";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css"
import Swal from "sweetalert2";

const New = () => {
  const dispatch = useDispatch();

  const [error, setError] = useState(true)
  const [input, setInput] = useState({
    title: "",
    body: "",
  });

  const handleChange = (e) => {
    if (input.body.length >= 5 && input.title.length >= 5) {
      setError(false)
    } else {
      setError(true)
    }
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  console.log(error, input)

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(createPost(input)).then(() => {
      Swal.fire("Post created!", "The post was created.", "success");
      setInput({ title: "", body: "" })
    })
  };

  return (
    <Fragment>
      <div className="card text-white bg-secondary mb-3">
        <form onSubmit={handleSubmit}>
          <div className="mb-3 row">
            <label className="col-sm-2 col-form-label">Title</label>
          </div>
          <div className="col-sm-10">
            <input value={input.title}
              name="title"
              onChange={handleChange}
              type="text"
              min="5"
              max="20" />
          </div>
          <div className="mb-3 row">
            <label className="col-sm-2 col-form-label">Body</label>
          </div>
          <div className="col-sm-10">
            <textarea
              value={input.body}
              name="body"
              onChange={handleChange}
              type="text"
              min="5"
              max="200"
            />
          </div>
          <button
            disabled={
              error
            }
            type="submit"
            className="btn btn-success"
          >
            New Post
          </button>
        </form>
      </div>
    </Fragment>
  );
};

export default New;