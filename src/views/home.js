import { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styles from "./css/styles.css";
import Card from "../components/Card";
import { fetchAllPosts } from "../reducers/postsReducer";

const Home = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllPosts());
  }, [dispatch]);

  const { posts } = useSelector((state) => state.posts);

  return (
    <Fragment>
      <div className={styles.gridContainer}>
        <main className={styles.grid}>
          {posts &&
            posts.length > 0 &&
            posts.map((post) => {
              return <Card post={post} />;
            })}
        </main>
      </div>
    </Fragment>
  );
};

export default Home;
